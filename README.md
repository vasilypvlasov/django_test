**Django test task for milo**

Install:

- `git clone git@gitlab.com:vasilypvlasov/django_test.git`
- Go to project dir
- Create venv(python 3.5.0)
- `pip install -r requirements.txt`
- `python manage.py migrate`
- `python manage.py runserver`
- open in browser http://127.0.0.1:8000/user/list

For running tests type in terminal `python manage.py test`