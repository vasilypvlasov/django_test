import csv

from django.http import HttpResponse
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import ListView
from django.core.urlresolvers import reverse_lazy
from django.contrib import messages
from django.utils.translation import ugettext as _


from .forms import UserProfileCreationForm
from .models import UserProfile
from .utils import get_eligible, get_bizz_fuzz


class Index(object):
    template_name = 'index.html'


class SuccessMessage(object):

    def get_success_url(self):
        if getattr(self, 'initial', None):
            messages.success(self.request, _('User updated'))
        else:
            messages.success(self.request, _('User added'))
        return reverse_lazy('list')


class UserProfileCreateView(SuccessMessage, CreateView):
    form_class = UserProfileCreationForm
    template_name = 'form.html'


class UserProfileListView(ListView):
    model = UserProfile
    template_name = 'list.html'
    context_object_name = 'profile_list'


class UserProfileUpdateView(SuccessMessage, UpdateView):
    '''Can change birthday
    '''
    model = UserProfile
    template_name = 'form.html'

    fields = ['birthday']


class UserProfileDeleteView(DeleteView):
    model = UserProfile

    def get_success_url(self):
        messages.success(self.request, _('User deleted'))
        return reverse_lazy('list')


def export_profile_users_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="profileusers.csv"'

    writer = csv.writer(response)
    writer.writerow(['Username', 'Birthday', 'Eligible', 'Random number', 'BizzFuzz'])

    users = UserProfile.objects.all()
    for user in users:
        prepared_user = [user.user.username, user.birthday, get_eligible(user.birthday), user.random_number, get_bizz_fuzz(user.random_number)]
        writer.writerow(prepared_user)

    return response
