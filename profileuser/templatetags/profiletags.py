from django import template
from datetime import date

register = template.Library()


@register.simple_tag
def bizzfuzz(random_number):
    if random_number % 3 == 0:
        return 'BizzFuzz' if (random_number % 5 == 0) else 'Fuzz'
    if random_number % 5 == 0:
        return 'Bizz'
    else:
        return random_number


@register.simple_tag
def check_age(birthday):
    # assuming year has 365 days, not perfect though
    if int((date.today() - birthday).days / 365) > 13:
        return 'allowed'
    else:
        return 'blocked'
