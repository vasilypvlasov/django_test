from django.db import models
from django.utils.translation import ugettext as _
from django.contrib.auth.models import User


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    birthday = models.DateField(help_text=_("In YYYY-MM-DD format"), null=True)
    random_number = models.IntegerField()

    def delete(self):
        '''Deletes user when profile deleting
        '''
        user = self.user
        super(UserProfile, self).delete()
        user.delete()

    def __str__(self):
        return '{}/{} {}'.format(self.user, self.birthday, self.random_number)
