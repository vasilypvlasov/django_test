import unittest

from django.contrib.auth.models import User
from .models import UserProfile
from .utils import get_eligible, get_bizz_fuzz


class OldTestCase(unittest.TestCase):

    def setUp(self):
        self.u1 = User.objects.create_user(username='user1')
        self.up1 = UserProfile.objects.create(user=self.u1, birthday='2010-05-01', random_number=1)

        self.u2 = User.objects.create_user(username='user2')
        self.up2 = UserProfile.objects.create(user=self.u2, birthday='1995-11-25', random_number=2)

    def tearDown(self):
        self.up1.delete()
        self.up2.delete()

    def test_user_old(self):
        u1 = UserProfile.objects.get(user=self.u1)
        u2 = UserProfile.objects.get(user=self.u2)
        self.assertEqual(get_eligible(u1.birthday), 'blocked')
        self.assertEqual(get_eligible(u2.birthday), 'allowed')


class BizzFuzzTestCase(unittest.TestCase):

    def setUp(self):
        self.u1 = User.objects.create_user(username='user1')
        self.up1 = UserProfile.objects.create(user=self.u1, birthday='2010-05-01', random_number=4)

        self.u2 = User.objects.create_user(username='user2')
        self.up2 = UserProfile.objects.create(user=self.u2, birthday='1995-11-25', random_number=6)

        self.u3 = User.objects.create_user(username='user3')
        self.up3 = UserProfile.objects.create(user=self.u3, birthday='1995-11-25', random_number=10)

        self.u4 = User.objects.create_user(username='user4')
        self.up4 = UserProfile.objects.create(user=self.u4, birthday='1995-11-25', random_number=30)

    def tearDown(self):
        self.up1.delete()
        self.up2.delete()
        self.up3.delete()
        self.up4.delete()

    def test_user_old(self):
        u1 = UserProfile.objects.get(user=self.u1)
        u2 = UserProfile.objects.get(user=self.u2)
        u3 = UserProfile.objects.get(user=self.u3)
        u4 = UserProfile.objects.get(user=self.u4)
        self.assertEqual(get_bizz_fuzz(u1.random_number), 4)
        self.assertEqual(get_bizz_fuzz(u2.random_number), 'Fuzz')
        self.assertEqual(get_bizz_fuzz(u3.random_number), 'Bizz')
        self.assertEqual(get_bizz_fuzz(u4.random_number), 'BizzFuzz')
