from datetime import date


def get_eligible(birthday):
    if int((date.today() - birthday).days / 365) > 13:
        return 'allowed'
    else:
        return 'blocked'


def get_bizz_fuzz(random_number):
    if random_number % 3 == 0:
        return 'BizzFuzz' if (random_number % 5 == 0) else 'Fuzz'
    if random_number % 5 == 0:
        return 'Bizz'
    else:
        return random_number
